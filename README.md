# spring security

delimiter $$

CREATE TABLE `users` (
  `username` varchar(45) NOT NULL,
  `password` varchar(500) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$



INSERT INTO `bankallotment_db`.`users`
(`username`,
`password`,
`enabled`)
VALUES
(
{username: VARCHAR},
{password: VARCHAR},
{enabled: TINYINT}
);


delimiter $$

CREATE TABLE `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(50) NOT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `ix_auth_username` (`username`,`authority`),
  CONSTRAINT `fk_authorities_users` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$



INSERT INTO `bankallotment_db`.`authorities`
(`username`,
`authority`)
VALUES
(
{username: VARCHAR},
{authority: VARCHAR}
);


