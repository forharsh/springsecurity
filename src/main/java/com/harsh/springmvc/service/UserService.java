package com.harsh.springmvc.service;

import java.util.List;

import com.harsh.springmvc.model.User;
import org.springframework.security.access.prepost.PreAuthorize;


public interface UserService {
	
	User findById(long id);
	
	User findByName(String name);
	
	void saveUser(User user);
	
	void updateUser(User user);
	
	void deleteUserById(long id);

	@PreAuthorize("hasRole('ADMIN')")
	List<User> findAllUsers(); 
	
	void deleteAllUsers();
	
	public boolean isUserExist(User user);
	
}
