package com.harsh.springmvc.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;
	
	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
		authenticationManagerBuilder
				.authenticationEventPublisher(new AuthenticationEventPublisher() {
					@Override
					public void publishAuthenticationSuccess(Authentication authentication) {
						System.out.println("SUCCESS");
					}

					@Override
					public void publishAuthenticationFailure(AuthenticationException e, Authentication authentication) {
						System.out.println("FAILURE");
					}
				})
				.jdbcAuthentication()
				.dataSource(dataSource);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
 
	  http.csrf()
			  .disable()
			  .sessionManagement()
			  .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			  .and()
			  .authorizeRequests()
	  		  .antMatchers("/db/").hasRole("ADMIN")
			  .antMatchers( "/user/**").hasRole("HARSH")
		      .and().httpBasic();
 	}
	

}
